<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sondage IC tableaux questionnaire</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/sideNavStyle.css" />" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
</head>
<body>

<body>
  <div class="container">
  <c:import url="header.jsp" />
     <div class="row">
       	<div class="col-lg-2">
        <!--  Sidenav partial -->
        <c:import url="partials/admin-sidenav.jsp" />
        </div>
     
        <div class="col-lg-9 col-lg-push-0">
		 <!-- Main -->
		    <div class="main">
              <h2>Tableaux Questionnaires (3 tableaux)</h2>
              
              <!--  TABLEAU QUESTIONS -->
              <h3 style="color:#0080ff">Tableau Questions</h3>
              <table class="table users admin-list">
				<thead>
<!--                    LISTE DE TOUS LES QUESTIONS -->
				  <c:forEach var = "q" begin = "0" end = "7">
					<tr>					
					   <td>QUESTION N�${q+1}</td>
					   <td class="text-left" >${listeQuestionReponses[q].question}</td> 
					</tr>
				  </c:forEach>
			    </thead>
			  </table>
				
              
              
              <!--  TABLEAU REPONSES -->
              <h3 style="color:#0080ff">Tableau R�ponses</h3>
              <table class="table users admin-list">
				<thead>
				 <tr>
					<th>ID</th>
					<c:forEach var = "q" begin = "1" end = "8">
						<th>QUESTION N�${q}</th>
					</c:forEach>
				</tr>
				</thead>
				 <tbody>
				   <!--  LISTE DE TOUS LES REPONSES DANS LA BDD -->
				   <c:forEach items="${listeQuestionnaires}" var="questionnaire">
					  <tr>
					   <td>${questionnaire.id}</td>
						    <c:forEach var = "q" begin = "0" end = "7">
							    <td>${questionnaire.listeQuestionReponses[q].reponse}</td>
						    </c:forEach>
					   </tr>
				    </c:forEach>	
                 </tbody>
               </table>
              
              <!--  TABLEAU COMMENTAIRES -->
              <h3 style="color:#0080ff">Tableau Commentaires</h3>
              <table  class="table" style="table-layout:fixed;" >
				<thead>
				 <tr>
					<th>ID</th>
					<th >COMMENTAIRE</th>					
				</tr>
				</thead>
				 <tbody>
				<!--  LISTE DE TOUS LES COMMENTAIRES DANS LA BDD -->
				<c:forEach items="${listeQuestionnaires}" var="questionnaire">
					<tr>
					<td >${questionnaire.id}</td>
					<td valign="top" class="commentaire">${questionnaire.commentaire}</td>	
					</tr>
				</c:forEach>	
              </tbody> 
            </table>           
              
        </div>
       </div> 
      </div>
     </div>     
</body>
</html>