<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html>
<html>
<head>
<title><spring:message code="accueil.home" /></title>
<!--  <meta name="viewport" content="width=device-width, initial-scale=1">-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<c:url value="/static/css/accueilStyle.css" />"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 

</head>

<body >
	<c:import url="header.jsp" />
	<br>
	
	<c:if test="${message == 'inscriptionOK'}">
	  <div align="center" ><i class="fa fa-check-square-o" style="font-size:36px;color:green"></i></div>
      <div class="info">Vous �tes bien inscrit, un email de confirmation vous a �t� envoy�.<br>Veuillez maintenant vous identifier.</div>      
	</c:if>

	 <c:if test="${message == 'erreurIdentification'}">
		  <div class="error">Nous avons detect� une erreur.<br>Votre mot de passe et/ou votre login est incorrect.</div>               
	 </c:if>
	  
	 <c:if test="${message == 'messageMailKo'}">
	   <div align="center" ><span class="glyphicon glyphicon-alert" align="center" style="font-size:36px;color:red"></span> </div>
	   <div class="error">Il n'y a pas de compte enregistr� avec cette adresse Email.<br>V�rifier votre Email ou inscrivez-vous sur le site.</div>      
	 </c:if>
	      
	 <c:if test="${message == 'messageMailOk'}">
	   <div align="center" ><i class="fa fa-check-square-o" style="font-size:36px;color:green"></i></div>
	   <div class="info">Un Email contenant un lien permettant la r�initialisation <br>de votre mot de passe est envoy� � l'adresse indiqu�e.</div>      
	 </c:if>
	      
	 <c:if test="${message == 'messageMotDePasseReinitialise'}">
	   <div align="center" ><i class="fa fa-check-square-o" style="font-size:36px;color:green"></i></div>
	   <div class="info">Votre mot de passe a �t�  r�initialis�. </div>     
	 </c:if>
   
    <c:if test="${message == 'erreurConnection'}">
		<div class="error">Vous n'�tes pas connect�.<br> Merci de vous identifier.</div>               
	</c:if>
	
	<c:if test="${message == 'messageMotDePasseModifie'}">
	           <div align="center" ><i class="fa fa-check-square-o" style="font-size:36px;color:green"></i></div>
	           <div class="info">Votre mot de passe a �t�  modifi�. </div>     
	     </c:if>		
	
	    <div class="jumbotron">
	         <div class="slogan">
			Un questionnaire destin� aux consultants Ausy organis� par Jean-Pascal Ferry pour �valuer l'Unit� de Recherche � Ausy.  
			</div>
		</div>
<br><br>
	<!--IMAGE -->
	

<div >
	<img id="imageAccueil" src="<c:url value="/static/images/imageAccueil.jpg"/>" alt="imageAccueil"
							width="400px" class ="center"/>

</div>
	
	<!--  Footer -->
	<c:import url="partials/footer.jsp" />
	
  <div id="connexionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	
</body>
</html>