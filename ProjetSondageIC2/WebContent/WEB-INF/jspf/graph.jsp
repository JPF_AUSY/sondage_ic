<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sondage IC graph</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/sideNavStyle.css" />" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
</head>

<body>
  <div class="container">
  <c:import url="header.jsp" />
     <div class="row">
       	<div class="col-lg-2">
        <!--  Sidenav partial -->
        <c:import url="partials/admin-sidenav.jsp" />
        </div>
     
        <div class="col-lg-9 col-lg-push-0">
		 <!-- Main -->
		    <div class="main">

            <h1>Graphiques camembert - Sondage Innovation Center</h1>


            <!-- R�cup�ration des donn�es du controller (listeQuestion, listeIntituleReponses et tableau nombre de r�ponse -->

            <!-- Boucle sur les questions -->
            <c:forEach var = "q" begin = "0" end = "7">
                <h2><p hidden id="question<c:out value = "${q}"/>">${listeQuestions[q]}</p></h2>
                <!-- Boucle sur les intitul�es des r�ponses -->
                <c:forEach var = "r" begin = "0" end = "6">
                   <p  hidden id="intitule<c:out value = "${r}"/>">${listeIntituleReponses[r]}</p>
                   <p  hidden id="nombreReponse<c:out value = "${q}"/><c:out value = "${r}"/>">${nombreReponse[q][r]}</p>
               </c:forEach>
            </c:forEach>  

            <!-- Emplacement pour les graphs pour questions 1 � 8 --> 
            <c:forEach var = "i" begin = "0" end = "7"> 
                 <div id="piechart<c:out value = "${i}"/>"></div> 
            </c:forEach> 

            <!-- JAVA SCRIPT GRAPH PIE CHART -->
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js">
            </script> 

            <script type="text/javascript"> 
               // Load google charts
                google.charts.load('current', {'packages':['corechart']});
 
               //Appel de la fonction de Dessin des graphs nombreR�ponses pour les questions 1 � 8 sur les �l�ments du DOM piechart
               for (q = 0; q < 7 ; i++){
                 google.charts.setOnLoadCallback(function(){ drawChart("nombreReponse"+q, "piechart"+q) });
                }     

               // Draw the chart and set the chart values
               function drawChart() {
	            //var i= 1
	            for (i = 0; i < 8 ; i++){	
                  var data = google.visualization.arrayToDataTable([
                   ['Task', 'Hours per Day'],
                   //Couple de donn�es : � gauche intitul� de la r�ponse, � droite nombre de r�ponses correspondantes 
                   [document.getElementById("intitule0").innerHTML, Number(document.getElementById("nombreReponse"+i+"0").innerHTML)],
                   [document.getElementById("intitule1").innerHTML, Number(document.getElementById("nombreReponse"+i+"1").innerHTML)],
                   [document.getElementById("intitule2").innerHTML, Number(document.getElementById("nombreReponse"+i+"2").innerHTML)],
                   [document.getElementById("intitule3").innerHTML, Number(document.getElementById("nombreReponse"+i+"3").innerHTML)],
                   [document.getElementById("intitule4").innerHTML, Number(document.getElementById("nombreReponse"+i+"4").innerHTML)],
                   [document.getElementById("intitule5").innerHTML, Number(document.getElementById("nombreReponse"+i+"5").innerHTML)],
                  ]);
 
                 // Optional; add a title and set the width and height of the chart
                 var options = {'title':document.getElementById("question"+i).innerHTML,
		             titleTextStyle: {color: '#000000', fontSize: 17 },
		             legend: {textStyle: {color: '#000000', fontSize: 15}},
		             'width':1000, 'height':400};
   
                // Display the chart inside the <div> element with id="piechart"
                var chart = new google.visualization.PieChart(document.getElementById('piechart'+i));
                    chart.draw(data, options);
	           }
           }
        </script> 

	</div>
   </div>
  </div>
 </div>

</body>
</html>