<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!-- Side navigation -->
<div class="sidenav">


	<a id ="profil" href="<c:url value="/donneesVisiteur/goToExperience" />">Profil</a>


	<a href="<c:url value="/visiteur/goToSelection" />">Selection des informations � afficher</a> 
	




	<a id = "choixtemplate" href="<c:url value="/visiteur/goToChoixTemplate" />">Choix du template</a> 
	

</div>
<%-- 
	                                          <!-- POPUP MODIFIER LE MOT DE PASSE -->
                                      
        <div id="changerMotDePasse" class="modal fade" role="dialog" >
        <form method="POST" action="<c:url value="/visiteur/changerMotDePasse" />" modelAttribute="credential">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <form:hidden path="credential.id" />
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Modifier le mot de passe</h4>                                    
                    </div>
                    <div class="modal-body">                    
                                       
	                <div id="connexionModal" class="form-group">
		             <label for="email"><spring:message code="credential.password.user" /></label>
		               <div class="input-group">
			             <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			             <input type="password" name="motDePasse" path="credential.password" value="" placeholder="Mot de passe actuel" class="form-control" required="required">
		               </div>
	                </div>
	                
	                <div id="connexionModal" class="form-group">
		             <label for="email"><spring:message code="credential.password.new" /></label>
		               <div class="input-group">
			             <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			             <input type="password" class="form-control"  name="nouveauMotDePasse" required placeholder="Nouveau mot de passe" class="form-control"
			             minlength="6" required="required" pattern="[0-9]{6,}" >
		               </div>
	                </div>
	                
	                <div id="connexionModal" class="form-group">
		             <label for="email"><spring:message code="credential.confirmationMotDePasse" /></label>
		               <div class="input-group">
			             <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			             <input type="password" class="form-control"  name="confirmationMotDePasse" required placeholder="Nouveau mot de passe" class="form-control"
			             minlength="6" required="required" pattern="[0-9]{6,}">
			             
		               </div>
	                </div>
	                
	                 <div align="center">
			             <tr><b><FONT color="blue">Format du mot de passe : 6 chiffres ou plus.</FONT></b></b></tr>
			       </div>	
	
	                <input type="submit" class="btn btn-danger" value="<spring:message code="credential.submit" />"/>
	                
	               	
	                
	             <c:if test="${message == 'messageMotsDePasseNonIdentiques'}">
		               <br>
		               <div align="center" ><span class="glyphicon glyphicon-alert" align="center" style="font-size:36px;color:red"></span> </div>
		               <h3 class="error" align="center">Attention, les deux nouveaux mots de passe rentr�s ne sont pas identiques !</h3> 
		               <script>
		               $("#changerMotDePasse").modal('show');
		               
		               </script>     
	             </c:if>
	             
	             <c:if test="${message == 'messageErreurMotDePasse'}">
		               <br>
		               <div align="center" ><span class="glyphicon glyphicon-alert" align="center" style="font-size:36px;color:red"></span> </div>
		               <h3 class="error" align="center">Attention, il y a une erreur sur le mot de passe actuel !</h3> 
		               <script>
		               $("#changerMotDePasse").modal('show');
		               
		               </script>     
	             </c:if>
	             
                   <br><br>
	                </div>
	                  </div>
                       </div>    
        </form>
        </div> --%>