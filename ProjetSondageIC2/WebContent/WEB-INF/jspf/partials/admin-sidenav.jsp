<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!-- Admin side navigation -->
<div class="admin-sidenav sidenav ">
	<ul>
	    <li><a href="<c:url value="/admin/goToGraph" />">Graphiques</a></li>
		<li><a href="<c:url value="/admin/questionnaires" />">Tableaux</a></li>				
	</ul>
</div>