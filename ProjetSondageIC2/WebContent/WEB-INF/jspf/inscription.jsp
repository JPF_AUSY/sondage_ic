<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>

<head>
  <title>Mon compte</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="<c:url value="/static/css/inscription.css" />" rel="stylesheet">
</head>

<body>

<div align="center" id="bg">
<img src="<c:url value="/static/images/blue-motion-blur-design.jpg" />" width=100% />
</div>

<div class="container">
	<div align="center">
		<h1>
		<font color="blue"><b><spring:message code="inscription.creation" /></b></font>
		</h1>
	</div>
	<div class="form-group" bgcolor="#E6E6FA">
	<form method="POST" action="creer" modelAttribute="utilisateur">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<form:hidden path="utilisateur.id" />
	<form:hidden path="utilisateur.credential.id" />
		<div align="center">
			<TABLE class="tableinscription" BORDER=0>
				<tr>
					<td nowrap><form:label path="utilisateur.credential.nomLogin">
					<spring:message code="utilisateur.credential.nomLogin" /><span class="required">*</span></form:label></td>
					<td><form:input path="utilisateur.credential.nomLogin" class="form-control" required="required" pattern="[^' ']+" 
					 oninvalid="setCustomValidity('Le caract�re espace n\\'est pas autoris�.')"  
                       oninput="setCustomValidity('')" /> 
					
					</td>
					<td><form:errors path="utilisateur.credential.nomLogin" cssClass="errors" /></td>
				</tr>
				
				<tr>
					<td nowrap><form:label path="utilisateur.credential.email">
					<spring:message code="utilisateur.credential.email" /><span class="required">*</span></form:label></td>
					<td><form:input type="email" id="email" name="email" path="utilisateur.credential.email" class="form-control" required="required" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$" /></td>
					<td><form:errors path="utilisateur.credential.email" cssClass="errors" /></td>
				</tr>
			
				<tr>
					<td nowrap><form:label path="utilisateur.credential.motDePasse">
					<spring:message code="credential.password" /><span class="required">*</span></form:label></td>					
		            <td><form:input type="password" id="motDePasse" name="motDePasse" path="utilisateur.credential.motDePasse" class="form-control" minlength="6" required="required" pattern="^{6,}"/></td>
		            
		            <td><form:errors path="utilisateur.credential.motDePasse" cssClass="errors"/></td>
				</tr>
				
			</TABLE>
			
			<div align="center">
			<tr><b><FONT color="blue">le mot de passe doit contenir au moins de 6 caract�res</FONT></b></b></tr>
			</div>			
			
			</div>
		</div>
		<div align="center">
			<input type="submit" id = "saveInscription" class="btn btn-primary" value="Valider" align="center" />&nbsp;&nbsp;&nbsp;
			<sec:authorize access="!hasRole('ROLE_CLIENT')"><a class="btn btn-danger" href="<c:url value="/welcome/goToAccueil" />" >Retour</a></sec:authorize>
			<sec:authorize access="hasRole('ROLE_CLIENT')"><a class="btn btn-danger" href="goToMenuClient">Retour</a></sec:authorize>
			<sec:authorize access="hasRole('ROLE_ADMIN')"><a class="btn btn-danger" href="goToMenuAdmin">Retour</a></sec:authorize>
			
		</div>
		
		
</div>

<script>
/**
 * Enable mouse cursor display
 */
//  function enableCursor() {
// 	  var seleniumFollowerImg = document.createElement("img");
// 	  seleniumFollowerImg.setAttribute('src', 'data:image/png;base64,'
// 	    + 'iVBORw0KGgoAAAANSUhEUgAAABQAAAAeCAQAAACGG/bgAAAAAmJLR0QA/4ePzL8AAAAJcEhZcwAA'
// 	    + 'HsYAAB7GAZEt8iwAAAAHdElNRQfgAwgMIwdxU/i7AAABZklEQVQ4y43TsU4UURSH8W+XmYwkS2I0'
// 	    + '9CRKpKGhsvIJjG9giQmliHFZlkUIGnEF7KTiCagpsYHWhoTQaiUUxLixYZb5KAAZZhbunu7O/PKf'
// 	    + 'e+fcA+/pqwb4DuximEqXhT4iI8dMpBWEsWsuGYdpZFttiLSSgTvhZ1W/SvfO1CvYdV1kPghV68a3'
// 	    + '0zzUWZH5pBqEui7dnqlFmLoq0gxC1XfGZdoLal2kea8ahLoqKXNAJQBT2yJzwUTVt0bS6ANqy1ga'
// 	    + 'VCEq/oVTtjji4hQVhhnlYBH4WIJV9vlkXLm+10R8oJb79Jl1j9UdazJRGpkrmNkSF9SOz2T71s7M'
// 	    + 'SIfD2lmmfjGSRz3hK8l4w1P+bah/HJLN0sys2JSMZQB+jKo6KSc8vLlLn5ikzF4268Wg2+pPOWW6'
// 	    + 'ONcpr3PrXy9VfS473M/D7H+TLmrqsXtOGctvxvMv2oVNP+Av0uHbzbxyJaywyUjx8TlnPY2YxqkD'
// 	    + 'dAAAAABJRU5ErkJggg==');
// 	  seleniumFollowerImg.setAttribute('id', 'selenium_mouse_follower');
// 	  seleniumFollowerImg.setAttribute('style', 'position: absolute; z-index: 99999999999; pointer-events: none; left:0; top:0');
// 	  document.body.appendChild(seleniumFollowerImg);
// 	  document.onmousemove = function (e) {
// 	    document.getElementById("selenium_mouse_follower").style.left = e.pageX;
// 	    document.getElementById("selenium_mouse_follower").style.top = e.pageY;
// 	  };
// 	};

// 	enableCursor();

</script>
</body>
</html>