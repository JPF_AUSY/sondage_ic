<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>Sondage IC utilisateur</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/sideNavStyle.css" />" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- <script -->
<!-- 	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<%-- <link href="<c:url value="/static/css/styles.css" />" rel="stylesheet"> --%>
</head>





<body>
		
<div class="container">
 <c:import url="header.jsp" />
  <div class="row">
                 <!-- 		<div class="col-lg-3"> -->
                 <!-- 		  Sidenav partial -->
                 <%-- 		  <c:import url="partials/utilisateur-sidenav.jsp" /> --%>
                  <!--         </div> -->
   <div class="col-lg-12 col-lg-push-0">
   <!-- Main -->
	<div class="main">
	
	
	
	  <h2 class="textBlack">Bienvenue <span class="nomlogin">${credential.nomLogin} </span></h2>			
      <h4 class="textBleuBlack">Voici le questionnaire organis� par Jean-Pascal Ferry pour �valuer l'Unit� de Recherche � Ausy :</h4>
      <c:if test="${message == 'messageMotDePasseModifie'}">
	    <div align="center" ><i class="fa fa-check-square-o" style="font-size:36px;color:green"></i></div>
	    <div class="info">Votre mot de passe a �t�  modifi�. </div>     
	  </c:if>	
      <div>
	
      <!--********************************************-->	
      <!--**             QUESTIONNAIRE              **-->
      <!--********************************************-->      
      <form method="POST" action="creerQuestionnaire"	modelAttribute="questionnaire"> 
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
      <form:hidden path="questionnaire.id" /> 
    
      
      <!-- BOUTONS RADIO --> 
      <!--  Boucle Questions -->
       <c:forEach items="${listeQuestionReponses}" var="questionReponse"  varStatus="loopQuestion">
         <div>  <h2 style="color:#0080ff">${questionReponse.question}  </h2>
           <%--  On rentre chaque Question sous forme de formulaire cach�          --%>
           <!-- la question est transf�r�e vers la classe questionnaire via l'attribut "question" de l'�l�ment 
           <!-- de la "listeQuestionR�ponse" correspondant � l'index de la boucle sur les questions --> 
           <form:hidden path="questionnaire.listeQuestionReponses[${loopQuestion.index}].question" value="${questionReponse.question}" /> 
              
            <!--  Boucle Reponses - liste sur les diff�rentes r�ponses possibles    -->
 	        <c:forEach items="${listeIntituleReponses}"  var="intituleReponse" varStatus="loopReponse" >
 	          <label class="containerCheckbox">  ${intituleReponse} 
                <!-- la r�ponse du candidat d�termin�e par le choix du bouton est transf�r�e vers la classe questionnaire -->
                <!-- via l'attribut "r�ponse" de l'�l�ment de la "listeQuestionR�ponse" correspondant � l'index de la boucle sur les questions -->
  	            <form:radiobutton path="questionnaire.listeQuestionReponses[${loopQuestion.index}].reponse" name="radio${loopQuestion.index}" value="${intituleReponse}" required="required"/> 
                <span class="checkmarkCheckbox"></span>
	          </label> 
	        </c:forEach>
<!-- 	        <br> -->
	       </div>  
         </c:forEach>
         <div>
         <br>
        <h3>Commentaires :</h3> 
           <form:textarea  value="" rows="5" path="questionnaire.commentaire" placeholder="Ecrivez vos commentaires ici" class="form-control" /> 
         </div>
         <br>
         <div>  <input type="submit" class="btn btn-primary" value="Valider"align="center" /></div>
         <br>
       </form>
      </div>
	 </div>
    </div>
  </div>
 </div>
 
 <div id="connexionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
</body>
</html>