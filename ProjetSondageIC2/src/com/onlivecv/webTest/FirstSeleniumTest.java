package com.onlivecv.webTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class FirstSeleniumTest {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", "data\\geckodriver.exe");
		driver = new FirefoxDriver();

		// INSCRIPTION (Attention simulation s'arrete si utilisateur dans la base,
		// RAJOUTER CODE POUR SUPPRIMER L'UTILISATEUR DE LA BDD ROLE ADMIN)
		// driver.findElement(By.id("anim1")).click();
		// TimeUnit.SECONDS.sleep(1);

		////// on insere le nom
		// driver.findElement(By.id("credential.nomLogin")).click();
		// driver.findElement(By.id("credential.nomLogin")).clear();
		// driver.findElement(By.id("credential.nomLogin")).sendKeys("new_user");
		// TimeUnit.SECONDS.sleep(1);
		//// on insere l'email
		// driver.findElement(By.id("email")).click();
		// driver.findElement(By.id("email")).clear();
		// driver.findElement(By.id("email")).sendKeys("newuser@user.org");
		// TimeUnit.SECONDS.sleep(1);
		//// on insere le mdp
		// driver.findElement(By.id("motDePasse")).click();
		// driver.findElement(By.id("motDePasse")).clear();
		// driver.findElement(By.id("motDePasse")).sendKeys("123456");
		// TimeUnit.SECONDS.sleep(1);
		// driver.findElement(By.id("saveInscription")).click();
		// TimeUnit.SECONDS.sleep(1);

		// "S'IDENTIFIER"

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://localhost:8080/ProjetOnlineCv/securitycontroller/login");
		driver.findElement(By.id("ident")).click();
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys("jjstone");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("123456");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("validateLogin")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("profil")).click();
		TimeUnit.SECONDS.sleep(1);

		// "INFORMATIONS PERSONNELLES"

		driver.findElement(By.id("infoPersonnellesFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("nom")).clear();
		driver.findElement(By.id("nom")).sendKeys("Johnstone");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("prenom")).clear();
		driver.findElement(By.id("prenom")).sendKeys("John");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("datefield")).clear();
		driver.findElement(By.id("datefield")).sendKeys("1980-01-19");
		TimeUnit.SECONDS.sleep(1);

		// dropdown list nationalité
		Select dropdown1 = new Select(driver.findElement(By.id("nationalite_1")));
		dropdown1.selectByVisibleText("FRANCAIS");

		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("telephone")).clear();
		driver.findElement(By.id("telephone")).sendKeys("0102030405");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("adresse.rue")).clear();
		driver.findElement(By.id("adresse.rue")).sendKeys("Alpha Omega");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("adresse.codePostal")).clear();
		driver.findElement(By.id("adresse.codePostal")).sendKeys("31000");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("adresse.ville")).clear();
		driver.findElement(By.id("adresse.ville")).sendKeys("Paris");
		TimeUnit.SECONDS.sleep(1);

		// dropdown list nomPays
		Select dropdown2 = new Select(driver.findElement(By.id("adresse.nomPays")));
		dropdown2.selectByVisibleText("FRANCE");

		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s0")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("closeAccInfoPerso")).click();

		// "TITRES"

		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("titresFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("titre")).sendKeys("WEB DEVELOPER");
		driver.findElement(By.id("s1")).click();
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("titre")).sendKeys("CONCEPTEUR BE");
		driver.findElement(By.id("s1")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("closeAccTitre")).click();

		// "LANGUES"

		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("languesFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);

		// dropdown list nomLangue
		Select dropdown3 = new Select(driver.findElement(By.id("nomLangue.id")));
		dropdown3.selectByVisibleText("ANGLAIS");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s3")).click();

		Select dropdown4 = new Select(driver.findElement(By.id("niveau.id")));
		dropdown4.selectByVisibleText("Professionnel");
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("s3")).click();
		TimeUnit.SECONDS.sleep(1);

		Select dropdown5 = new Select(driver.findElement(By.id("nomLangue.id")));
		dropdown5.selectByVisibleText("ESPAGNOL");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s3")).click();

		Select dropdown6 = new Select(driver.findElement(By.id("niveau.id")));
		dropdown6.selectByVisibleText("Notions");
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("s3")).click();
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("closeAccLangues")).click();

		// "FORMATIONS"

		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("formationsFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("intitule")).clear();
		driver.findElement(By.id("intitule")).sendKeys("DUT GENIE MECANIQUE");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("formation-datepicker")).clear();
		driver.findElement(By.id("formation-datepicker")).sendKeys("1998");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("valider")).click();
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("closeAccFormations")).click();

		// "COMPETENCES"

		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("competencesFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("comp1")).clear();
		driver.findElement(By.id("comp1")).sendKeys("JAVA");
		TimeUnit.SECONDS.sleep(1);

		Select dropdown7 = new Select(driver.findElement(By.id("n1")));
		dropdown7.selectByVisibleText("Confirme");
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("s4")).click();
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("comp1")).clear();
		driver.findElement(By.id("comp1")).sendKeys("ANGULAR");
		TimeUnit.SECONDS.sleep(1);

		Select dropdown8 = new Select(driver.findElement(By.id("n1")));
		dropdown8.selectByVisibleText("Expert");
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("s4")).click();
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("fermerAccCompetence")).click();

		// "CERTIFICATIONS"

		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("certificationsFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("certif1")).clear();
		driver.findElement(By.id("certif1")).sendKeys("CIMPEC");
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("certification-date")).clear();
		driver.findElement(By.id("certification-date")).sendKeys("Avril 2010");
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("score")).clear();
		driver.findElement(By.id("score")).sendKeys("8");
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("s5")).click();
		TimeUnit.SECONDS.sleep(1);

		driver.findElement(By.id("fermerAccCertification")).click();

		// driver.findElement(By.id("choixtemplate")).click();
		// driver.findElement(By.id("tmpselection")).click();

	}
}
