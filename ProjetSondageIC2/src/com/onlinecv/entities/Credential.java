package com.onlinecv.entities;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.springframework.security.core.GrantedAuthority;

import com.onlinecv.config.Principal;

@Entity
public class Credential {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty(message = "{error.credential.obligatoire}")
	@Column(unique = true, nullable = false)
	private String nomLogin;
	@NotEmpty(message = "{error.credential.obligatoire}")
	@Email(message = "{error.credential.email.malforme}")
	private String email;
	@NotEmpty(message = "{error.credential.obligatoire}")
	private String motDePasse;
	@Enumerated(EnumType.STRING)
	private ERole role;

	public Credential() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomLogin() {
		return nomLogin;
	}

	public void setNomLogin(String nomLogin) {
		this.nomLogin = nomLogin;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public ERole getRole() {
		return role;
	}

	public void setRole(ERole role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Credential [id=" + id + ", nomLogin=" + nomLogin + ", email=" + email + ", motDePasse=" + motDePasse
				+ ", role=" + role + "]";
	}

	public Principal getPrincipal() {
		// TODO Auto-generated method stub
		return null;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

}
