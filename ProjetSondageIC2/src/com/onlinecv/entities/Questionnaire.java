package com.onlinecv.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Questionnaire {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne
	private Credential credential;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<QuestionReponse> listeQuestionReponses;
	
	@Column(columnDefinition="LONGTEXT")
	private String commentaire;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Credential getCredential() {
		return credential;
	}
	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	public List<QuestionReponse> getListeQuestionReponses() {
		return listeQuestionReponses;
	}
	public void setListeQuestionReponses(List<QuestionReponse> listeQuestionReponses) {
		this.listeQuestionReponses = listeQuestionReponses;
	}
	

	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	@Override
	public String toString() {
		return "Questionnaire [id=" + id + ", credential=" + credential + ", listeQuestionReponses="
				+ listeQuestionReponses + ", commentaire=" + commentaire + "]";
	}
		
}
