package com.onlinecv.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class QuestionReponse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String question;	
	private String reponse;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getReponse() {
		return reponse;
	}
	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
	@Override
	public String toString() {
		return "QuestionReponse [id=" + id + ", question=" + question + ", reponse=" + reponse + "]";
	}
	public QuestionReponse( String question) {
		super();
		this.question = question;
	}
	public QuestionReponse() {
		// TODO Auto-generated constructor stub
	}	
	
	
	
}
