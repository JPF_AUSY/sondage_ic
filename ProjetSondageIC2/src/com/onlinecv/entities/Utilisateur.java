package com.onlinecv.entities;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.Valid;

@Entity
public class Utilisateur  { 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	

	@Valid
	@OneToOne(cascade = CascadeType.ALL)
	private Credential credential;

	

	

	public Utilisateur() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	

	

	
	@Override
	public boolean equals(Object o) {
		Utilisateur other = (Utilisateur) o;
		return (this.getId() == other.getId());
	}

	@Override
	public int hashCode() {
		Integer hash = (int) (long) id;
		return hash;
	}

	@Override
	public String toString() {
		// return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ",
		// credential=" + credential
		// + ", dateDeNaissance=" + dateDeNaissance + ", adresse=" + adresse + ",
		// telephone=" + telephone
		// + ", nationalite_1=" + nationalite_1 + ", nationalite_2=" + nationalite_2 +
		// ", parcours=" + parcours
		// + ", photo=" + photo + "]";
		return "Utilisateur [id=" + id + "]";
	}

	
}
