//package com.onlinecv.entities;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//
//@Entity
//public class Question {
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;
//	private String intituleQuestion;
//	
//	public Long getId() {
//		return id;
//	}
//	public void setId(Long id) {
//		this.id = id;
//	}
//	public String getIntituleQuestion() {
//		return intituleQuestion;
//	}
//	public void setIntituleQuestion(String intituleQuestion) {
//		this.intituleQuestion = intituleQuestion;
//	}
//	@Override
//	public String toString() {
//		return "Question [id=" + id + ", intituleQuestion=" + intituleQuestion + "]";
//	}
//	public Question(String intituleQuestion) {
//		super();
//		this.intituleQuestion = intituleQuestion;
//	}
//	public Question(Long id, String intituleQuestion) {
//		super();
//		this.id = id;
//		this.intituleQuestion = intituleQuestion;
//	}
//	
//	
//	
//}
