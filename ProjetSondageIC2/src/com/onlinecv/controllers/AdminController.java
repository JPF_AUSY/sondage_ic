package com.onlinecv.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.DocumentException;
import com.onlinecv.dao.IAdminJpaRepository;
import com.onlinecv.dao.ICredentialJpaRepository;
import com.onlinecv.dao.IQuestionReponseJpaRepository;
import com.onlinecv.dao.IQuestionnaireJpaRepository;
import com.onlinecv.dao.IUtilisateurJpaRepository;
import com.onlinecv.entities.Credential;
import com.onlinecv.entities.ERole;

import com.onlinecv.entities.QuestionReponse;
import com.onlinecv.entities.Questionnaire;
import com.onlinecv.entities.Utilisateur;

@Controller
@RequestMapping("/admin")
@Secured("ROLE_ADMIN")
public class AdminController {

	// static String rootval = new String();

	// id de l'utilisateur résultat de la recherche
	private Long utilisateurIdPass;

	// identification de la page
	private String pageId;

	static String rootPathFinal = new String();

	@Autowired
	private ICredentialJpaRepository credentialRepo;

	
	@Autowired
	private IQuestionnaireJpaRepository questionnaireRepo;

	@Autowired
	private IQuestionReponseJpaRepository questionReponseRepo;
	
	

	private IAdminJpaRepository adminRepo;

	@Secured("ROLE_ADMIN")
	@RequestMapping("/goToGraph")
	public String gotoMenuAdmin(Model model) {
    Credential credential = credentialRepo.getOne(AuthHelper.getPrincipal().getCredential().getId());
	model.addAttribute("credential", credential);
	
			
	//Création tableau Question
	String[] listeQuestions = {"Comment évaluez-vous l'ambiance de travail ?",
			                  "Comment évaluez-vous votre encadrement ?",
			                  "Comment évaluez-vous votre relation avec le responsable de l'Unité de Recherche ?",
			                  "Comment évaluez-vous l'environnement de travail en termes de qualité des équipements ?",
			                  "Comment vous évaluez-vous en terme de montée en compétence ?",
			                  "Comment évaluez-vous la charge de travail ?",
			                  "Comment évaluez-vous l'intérêt des projets proposés ?",
			                  "Comment évaluez-vous votre autonomie sur les projets ?",};
	model.addAttribute("listeQuestions",listeQuestions);
	
	//Création tableau Réponses
	String[] listeIntituleReponses = {"Extrêmement bien","Très bien","Assez bien","Moyennement bien","Pas bien du tout ","Sans opinion"};
	model.addAttribute("listeIntituleReponses",listeIntituleReponses);
	
	//Création tableau Nombre de Réponses par question
	Long[][] nombreReponse = new Long[listeQuestions.length][listeIntituleReponses.length];
		
	
	//RECHERCHE NOMBRE DE REPONSES IDENTIQUES PAR QUESTION
	// Boucle sur les questions (0 à 7) index q	
	for(int q = 0; q < listeQuestions.length; q++)  {
		// Boucle sur les intitulés des réponses (0 à 5) index r
		for(int r = 0; r < listeIntituleReponses.length; r++) {
			nombreReponse[q][r] =questionReponseRepo.nombreReponse(listeQuestions[q], listeIntituleReponses[r]);
			System.out.println("nombre Reponse " +5 +"par question "+0+"= " +nombreReponse[0][5]);
		}
	}
	
	model.addAttribute("nombreReponse",nombreReponse);
	
return "graph";
	}

	
	@Secured("ROLE_ADMIN")
	@RequestMapping("/questionnaires")
	public String questionnaires(Model model) {
		// Liste de tous les questionnaires
		List<Questionnaire> listeQuestionnaires = questionnaireRepo.findAll();
		model.addAttribute("listeQuestionnaires", listeQuestionnaires);
		
		//Nombre total de questionnaires dans la DB
		System.out.println("nb total de questionnaires = "+listeQuestionnaires.size());
		
		List<QuestionReponse> listeQuestionReponses = questionReponseRepo.findAll();
		model.addAttribute("listeQuestionReponses", listeQuestionReponses);
		
		System.out.println("liste questionnaire = "+listeQuestionnaires);
		return "tableauxQuestionnaire";
	}



	
}
