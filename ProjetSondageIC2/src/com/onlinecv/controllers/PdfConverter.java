package com.onlinecv.controllers;

import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfConverter {
	public static void main(String[] args) throws IOException {
		// PDF creation is working and saving into project directory.
		// create empty A4 document
		Document document = new Document(PageSize.A4);
		String filename = "test.pdf";
		// document meta
		document.addAuthor("Ausy FR");
		document.addTitle("AUsy CV");
		System.out.println("Document created");
		try {
			PdfWriter.getInstance(document, new FileOutputStream(filename));
			System.out.println("writer instance created...");
			// open doc
			document.open();
			System.out.println("Document open...");
			// new p
			Paragraph p = new Paragraph("Hello world");
			// add p to doc
			document.add(p);
			System.out.println("P added to document...");

		} catch (Exception e) {
			System.out.println(e);
		}
		// close document
		document.close();

		// step 1

	}

}