package com.onlinecv.controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.hibernate.Session;
import org.hibernate.Transaction;
//import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.MailSender;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.itextpdf.io.IOException;
import com.itextpdf.text.DocumentException;
import com.mysql.fabric.xmlrpc.base.Array;
import com.onlinecv.dao.ICredentialJpaRepository;
import com.onlinecv.dao.IQuestionnaireJpaRepository;

import com.onlinecv.dao.IUtilisateurJpaRepository;
import com.onlinecv.entities.Credential;

import com.onlinecv.entities.ERole;

import com.onlinecv.entities.MailMail;

import com.onlinecv.entities.QuestionReponse;
import com.onlinecv.entities.Questionnaire;
import com.onlinecv.entities.Utilisateur;

@Controller
@RequestMapping("/visiteur")
public class UtilisateurController {

	private MailSender mailSender;

	@Autowired
	private IUtilisateurJpaRepository utilisateurRepo;

	@Autowired
	private ICredentialJpaRepository credentialRepo;
	

	
	@Autowired
	private IQuestionnaireJpaRepository questionnaireRepo;

	

	Session session = null;
	Transaction transaction = null;

	@Secured("ROLE_UTILISATEUR")
	@RequestMapping("/goToQuestionnaire")
	public String gotoMenuUtilisateur(Model model) {
		
	//	List<Utilisateur> ListeUtilisateurs = utilisateurRepo.findAll();
		Credential credential = credentialRepo.getOne(AuthHelper.getPrincipal().getCredential().getId());
		
		model.addAttribute("credential", credential);
		
		
		List<QuestionReponse> listeQuestionReponses = new ArrayList<QuestionReponse>();
		
		QuestionReponse questionReponse1 = new QuestionReponse("Comment évaluez-vous l'ambiance de travail ?");
		System.out.println("question 1 = "+questionReponse1);
		listeQuestionReponses.add(questionReponse1);
		System.out.println("question 11 = "+questionReponse1);
		QuestionReponse questionReponse2 = new QuestionReponse("Comment évaluez-vous votre encadrement ?");
		listeQuestionReponses.add(questionReponse2);
		QuestionReponse questionReponse3 = new QuestionReponse("Comment évaluez-vous votre relation avec le responsable de l'Unité de Recherche ?");
		listeQuestionReponses.add(questionReponse3);
		QuestionReponse questionReponse4 = new QuestionReponse("Comment évaluez-vous l'environnement de travail en termes de qualité des équipements ?");
		listeQuestionReponses.add(questionReponse4);
		QuestionReponse questionReponse5 = new QuestionReponse("Comment vous évaluez-vous en terme de montée en compétence ?");
		listeQuestionReponses.add(questionReponse5);
		QuestionReponse questionReponse6 = new QuestionReponse("Comment évaluez-vous la charge de travail ?");
		listeQuestionReponses.add(questionReponse6);
		QuestionReponse questionReponse7 = new QuestionReponse("Comment évaluez-vous l'intérêt des projets proposés ?");
		listeQuestionReponses.add(questionReponse7);
		QuestionReponse questionReponse8 = new QuestionReponse("Comment évaluez-vous votre autonomie sur les projets ?");
		listeQuestionReponses.add(questionReponse8);
		model.addAttribute("listeQuestionReponses",listeQuestionReponses);
		
	   //Liste des intitulées des réponses
		String[] listeIntituleReponses = {"Extrêmement bien","Très bien","Assez bien","Moyennement bien","Pas bien du tout ","Sans opinion"};
		model.addAttribute("listeIntituleReponses",listeIntituleReponses);

		//Envoi de la classe questionnaire au niveau de la page jsp questionnaire
		Questionnaire questionnaire = new Questionnaire();
		questionnaire.setListeQuestionReponses(listeQuestionReponses);
		model.addAttribute("questionnaire", questionnaire);
		System.out.println("questionnaire0= "+questionnaire);
		
		return "questionnaire";
	}
	
	@PostMapping("/creerQuestionnaire")
	public String creerQuestionnaire(@Valid @ModelAttribute(value = "questionnaire") Questionnaire questionnaire,
			 BindingResult result, Model model) {
	
		//Récupérartion du credendial courant
	   Credential credential = credentialRepo.getOne(AuthHelper.getPrincipal().getCredential().getId());
	   model.addAttribute("credential", credential);
	   
	   //Sauvegarde du questionnaire dans la base de données
	   questionnaire.setCredential(credential);
	   System.out.println("questionnaire = " +questionnaire);
	   questionnaireRepo.save(questionnaire);
	   
		return "finQuestionnaire";
	}
	
	@GetMapping("/goToCreer")
	public String goToCreer(Model model) {
		model.addAttribute("utilisateur", new Utilisateur());
		return "inscription";
	}

	@PostMapping("/creer")
	public String creer(@Valid @ModelAttribute(value = "utilisateur") Utilisateur utilisateur, BindingResult result,
			Model model) {

		if (!result.hasErrors()) {

			String nomLogin = utilisateur.getCredential().getNomLogin();
			String email = utilisateur.getCredential().getEmail();
			Credential other = null;
			Credential other1 = null;
			Utilisateur otherUtilisateur = null;
			Long id = utilisateur.getId();

			if (id == null) { // creation d'un compte

				other = credentialRepo.findByNomLogin(nomLogin);
				other1 = credentialRepo.findByEmail(email);

			} else { // modification d'un compte
				otherUtilisateur = utilisateurRepo.findUtilisateurByIdNotAndCredentialNomLogin(id, nomLogin);
			}

			if (other != null) {
				result.rejectValue("credential.nomLogin", "error.credential.nomLogin.doublon");
			}
			if (other1 != null) {
				result.rejectValue("credential.email", "error.credential.email.doublon");
			}
		}

		if (!result.hasErrors()) {

			encodePassword(utilisateur.getCredential());
			utilisateur.getCredential().setRole(ERole.ROLE_UTILISATEUR);

			// Envoi d'un mail de confirmation de l'inscription
			ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Mail.xml");
			MailMail mm = (MailMail) context.getBean("mailMail");
			String urlPageAccueil = "http://localhost:8080/ProjetSondageIC2";

			mm.sendMail("gestioncvausy@gmail.com", utilisateur.getCredential().getEmail(),
					"Inscription au site CV création",

					"Bonjour " + utilisateur.getCredential().getNomLogin()
							+ ", \n bienvenue sur notre site Sondage IC. \n Votre inscription a bien été prise en compte."
							+ "\n Vous pouvez maintenant vous identifier avec votre Login et votre Mot de passe, à partir du lien suivant. \n"
							+ urlPageAccueil + "\n\n Cordialement \n L'équipe Sondage IC");

			utilisateurRepo.save(utilisateur);

			model.addAttribute("utilisateur", new Utilisateur());
			if (AuthHelper.isAuthenticated())
				return "menuUtilisateur";
			else
				model.addAttribute("message", "inscriptionOK");
			return "accueil";
		} else {
			return "inscription";
		}
	}

	private static void encodePassword(Credential credential) {
		String rawPassword = credential.getMotDePasse();
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encodedPassword = encoder.encode(rawPassword);
		credential.setMotDePasse(encodedPassword);
	}

	@RequestMapping("/motDePasseOublie")
	public String motDePasseOublie(Model model, @ModelAttribute(value = "credential") Credential credential) {

		// V�rification que le mail renseigne corresponds a un mail utilisateur en
		// base
		if (credentialRepo.findByEmail(credential.getEmail()) == null) {
			model.addAttribute("message", "messageMailKo");
			return "accueil";
		}
		// Envoi d'un mail contenant le lien pour la modification du mot de passe
		else {
			model.addAttribute("message", "messageMailOk");// message d'information a utilisateur de l'envoi d'un mail
															// avec lien pour reinitialiser le mot de passe
			ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Mail.xml");
			MailMail mm = (MailMail) context.getBean("mailMail");
			Long id = credentialRepo.findByEmail(credential.getEmail()).getId();

			// login correspondant a l'Email renseigne dans le formulaire
			String login = (credentialRepo.findByEmail(credential.getEmail())).getNomLogin();
			String urlModifMotDePasse = "http://localhost:8080/ProjetSondageIC2/visiteur/goToReinitialiserMotDePasse?id="
					+ id;

			mm.sendMail("gestioncvausy@gmail.com", credential.getEmail(), "Réinitialisation du mot de passe Sondage IC",
					"Bonjour " + login + ", \n vous avez demandé de réinitialiser votre mot de passe."
							+ "\n Cliquez sur le  lien suivant pour la réinitialisation votre mot de passe. \n"
							+ urlModifMotDePasse + "\n \n Cordialement \n L'équipe Sondage IC");

			return "accueil";
		}
	}

	// MOT DE PASSE OUBLIE
	@RequestMapping("/goToReinitialiserMotDePasse")
	public String goToReinitialiserMotDePasse(Model model, @RequestParam Long id) {
		model.addAttribute("credential", new Credential());

		return "reinitialisationMotDePasse";
	}

	@RequestMapping("/goToSupprimerProfil")
	public String goToSupprimerProfil(Model model) {
		model.addAttribute("credential", new Credential());

		return "supprimer-profil";
	}

	// MOT DE PASSE OUBLIE    
	@RequestMapping("/reinitialiserMotDePasse")
	public String reinitialiserMotDePasse(@Valid @ModelAttribute(value = "credential") Credential credential,
			BindingResult result, Model model,
			@RequestParam(value = "confirmationMotDePasse") String confirmationMotDePasse, @RequestParam Long id) {

		String nomLogin = credential.getNomLogin();
		String motDePasse = credential.getMotDePasse();
		model.addAttribute("message", "messageMailKo");

		Long idFormulaire = null;
		// IdFormulaire est renseign� que si un login existant dans la base est
		// rentr�
		// par l'utilisateur
		if (credentialRepo.findByNomLogin(nomLogin) != null) {
			idFormulaire = (credentialRepo.findByNomLogin(nomLogin)).getId();
		}

		// Verification que le login renseigne existe dans la base de donnee
		if (idFormulaire == null) {
			model.addAttribute("message", "messageErreurLogin");
			return "reinitialisationMotDePasse";
		}
		// Verification que l'id correspondant au login renseign� dans le formulaire
		// correspond a l'id du mail envoye a l'utilisateur pour la reinitialisation
		// de son mot de passe
		if (id != idFormulaire) {
			model.addAttribute("message", "messageErreurLogin");
			return "reinitialisationMotDePasse";
		}

		// Vérification que le mot de passe et la confirmation du mot de passe sont
		// identiques.
		if (!motDePasse.equals(confirmationMotDePasse)) {
			model.addAttribute("message", "messageMotsDePasseNonIdentiques");
			return "reinitialisationMotDePasse";
		} else

		{
			// Modification du mot de passe pour le credential de l'utilisateur
			Credential credentialAModifier = credentialRepo.findByNomLogin(credential.getNomLogin());
			credentialAModifier.setMotDePasse(motDePasse);
			// Mot de passe encode
			encodePassword(credentialAModifier);
			// nouveau mot de passe sauve
			credentialRepo.save(credentialAModifier);
			model.addAttribute("message", "messageMotDePasseReinitialise");
			return "accueil";
		}
	}

	// MOT DE PASSE OUBLIE
	@RequestMapping("/goToChangerMotDePasse")
	public String goToChangerMotDePasse(Model model, @RequestParam Long id) {
		model.addAttribute("credential", new Credential());

		return "reinitialisationMotDePasse";
	}

	// CHANGEMENT MOT DE PASSE EN ETANT CONNECTE
	@RequestMapping("/changerMotDePasse")
	public String changerMotDePasse(@Valid @ModelAttribute(value = "credential") Credential credential,
			BindingResult result, @RequestParam(value = "nouveauMotDePasse") String nouveauMotDePasse,
			@RequestParam(value = "confirmationMotDePasse") String confirmationMotDePasse, Model model) {

		String motDePasseActuel = credential.getMotDePasse();
		// R�cup�ration du credential de l'utilisateur connecte
		Credential credentialConnecte = (AuthHelper.getUtilisateur().getCredential());
		String motDePasseDB = credentialConnecte.getMotDePasse();
		// Classe pour cryptage du mot de passe
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

		model.addAttribute("motDePasseActuel", motDePasseActuel);

		model.addAttribute("nouveauMotDePasse", nouveauMotDePasse);

		model.addAttribute("confirmationMotDePasse", confirmationMotDePasse);

		model.addAttribute("credential", credential);

		// V�rification que le mot de passe tape par l'utilisateur et celui de la
		// session (encode dans la base de donnees) sont identiques
		if (!encoder.matches(motDePasseActuel, motDePasseDB)) {
			model.addAttribute("message", "messageErreurMotDePasse");
			return "modificationMotDePasse";
		} else {
			// V�rification que le nouveau mot de passe et la confirmation du mot de passe
			// sont identiques.
			if (!nouveauMotDePasse.equals(confirmationMotDePasse)) {
				model.addAttribute("message", "messageMotsDePasseNonIdentiques");
				return "modificationMotDePasse";

			} else {
				// Modification du mot de passe pour le credential de l'utilisateur
				credentialConnecte.setMotDePasse(nouveauMotDePasse);
				// Mot de passe encode
				encodePassword(credentialConnecte);
				// nouveau mot de passe sauve
				credentialRepo.save(credentialConnecte);
				model.addAttribute("message", "messageMotDePasseModifie");
				return "modificationMotDePasse";
			}
		}
	}


	/**
	 * Verifier la validite d'un mail
	 */
	public static Boolean VerifMail(String email) {
		Boolean bValide = false;
		Pattern patTest = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$");
		Matcher matchTest = patTest.matcher(email.toUpperCase());
		bValide = matchTest.matches();
		return bValide;
	}
}