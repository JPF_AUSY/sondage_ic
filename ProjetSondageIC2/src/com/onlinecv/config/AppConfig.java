package com.onlinecv.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ImportResource("classpath:application-context.xml")
@ComponentScan(basePackages = { "com.onlinecv.dao", "com.onlinecv.services" })
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.onlinecv.dao" })
public class AppConfig {
	//
}
