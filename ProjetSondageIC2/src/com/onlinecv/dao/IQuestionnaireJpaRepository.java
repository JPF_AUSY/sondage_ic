package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.Questionnaire;


public interface IQuestionnaireJpaRepository extends JpaRepository<Questionnaire, Long>{
	
// @Query("SELECT COUNT(q) FROM Questionnaire q WHERE q.reponse1=:reponse1")
//	public Long nombreReponse1(@Param("reponse1") String reponse1);
//
// @Query("SELECT COUNT(q) FROM Questionnaire q WHERE q.reponse2=:reponse2")
//	public Long nombreReponse2(@Param("reponse2") String reponse2);
//
// @Query("SELECT COUNT(q) FROM Questionnaire q WHERE q.reponse3=:reponse3")
//	public Long nombreReponse3(@Param("reponse3") String reponse3);
// 
// @Query("SELECT COUNT(q) FROM Questionnaire q WHERE q.reponse4=:reponse4")
//	public Long nombreReponse4(@Param("reponse4") String reponse4);
// 
// @Query("SELECT COUNT(q) FROM Questionnaire q WHERE q.reponse5=:reponse5")
//	public Long nombreReponse5(@Param("reponse5") String reponse5);
// 
// @Query("SELECT COUNT(q) FROM Questionnaire q WHERE q.reponse6=:reponse6")
//	public Long nombreReponse6(@Param("reponse6") String reponse6);
// 
// @Query("SELECT COUNT(q) FROM Questionnaire q WHERE q.reponse7=:reponse7")
//	public Long nombreReponse7(@Param("reponse7") String reponse7);
// 
// @Query("SELECT COUNT(q) FROM Questionnaire q WHERE q.reponse8=:reponse8")
//	public Long nombreReponse8(@Param("reponse8") String reponse8);
	
}
