package com.onlinecv.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.ERole;
import com.onlinecv.entities.Utilisateur;

public interface IUtilisateurJpaRepository extends JpaRepository<Utilisateur, Long> {

	public Utilisateur findByCredentialNomLogin(String nomLogin);

	// methode de tri sur la liste de tous les utilisateurs, on affiche UNIQUEMENT
	// les UTILISATEURS avec le role "ROLE_UTILISATEUR"
	public List<Utilisateur> findByCredentialRole(ERole role);

	@Query("select c from Utilisateur c where c.id<>:id and c.credential.nomLogin=:nomLogin")
	public Utilisateur findUtilisateurByIdNotAndCredentialNomLogin(@Param("id") Long id,
			@Param("nomLogin") String nomLogin);


	// search by prenom | nom keyword
//	@Query("SELECT DISTINCT u from Utilisateur u where u.nom LIKE %:name% or u.prenom LIKE %:name%")
//	public List searchByName(@Param("name") String name);

}
