package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.QuestionReponse;
import com.onlinecv.entities.Questionnaire;

public interface IQuestionReponseJpaRepository extends JpaRepository<QuestionReponse, Long>{

	 @Query("SELECT COUNT(q) FROM QuestionReponse q WHERE q.question=:question AND q.reponse=:reponse")
		public Long nombreReponse(@Param("question") String question, @Param("reponse") String reponse);	
	
	
	
	
}
